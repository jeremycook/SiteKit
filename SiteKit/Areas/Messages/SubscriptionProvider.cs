﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace SiteKit.Areas.Messages
{
    public class SubscriptionProvider : ISubscriptionProvider
    {
        private readonly IServiceProvider serviceProvider;

        public SubscriptionProvider(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IEnumerable<ISubscription<TMessage>> GetSubscriptions<TMessage>(string topic) where TMessage : IMessage
        {
            var matches = serviceProvider.GetServices<ISubscription<TMessage>>();

            foreach (var sub in matches)
            {
                if (sub.GetType().GetCustomAttribute<TopicAttribute>() is TopicAttribute topicAttribute && topicAttribute.Topic == topic)
                {
                    yield return sub;
                }
                else
                {
                    yield return sub;
                }
            }
        }
    }
}
