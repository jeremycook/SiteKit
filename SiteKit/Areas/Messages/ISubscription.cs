﻿using System.Threading.Tasks;

namespace SiteKit.Areas.Messages
{
    public interface ISubscription<TMessage>
        where TMessage : IMessage
    {
        Task ProcessAsync(TMessage message);
    }
}