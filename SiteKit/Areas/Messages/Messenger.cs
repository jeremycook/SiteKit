﻿using SiteKit.Areas.Storage;
using System;
using System.Threading.Tasks;

namespace SiteKit.Areas.Messages
{
    public class Messenger : IMessenger
    {
        private readonly ISubscriptionProvider subscriptionProvider;

        public Messenger(ISubscriptionProvider subscriptionProvider)
        {
            this.subscriptionProvider = subscriptionProvider;
        }

        public async Task SendMessageAsync<TMessage>(string topic, TMessage message)
            where TMessage : IMessage
        {
            var subscriptions = subscriptionProvider.GetSubscriptions<TMessage>(topic);
            foreach (var sub in subscriptions)
            {
                await sub.ProcessAsync(message);
            }
        }
    }
}
