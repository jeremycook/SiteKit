﻿using System.Threading.Tasks;

namespace SiteKit.Areas.Messages
{
    public interface IMessenger
    {
        Task SendMessageAsync<TMessage>(string topic, TMessage message) where TMessage : IMessage;
    }
}