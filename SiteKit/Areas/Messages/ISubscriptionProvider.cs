﻿using System.Collections.Generic;

namespace SiteKit.Areas.Messages
{
    public interface ISubscriptionProvider
    {
        IEnumerable<ISubscription<TMessage>> GetSubscriptions<TMessage>(string topic)
            where TMessage : IMessage;
    }
}