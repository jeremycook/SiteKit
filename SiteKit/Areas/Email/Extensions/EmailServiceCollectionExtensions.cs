﻿using Microsoft.Extensions.Configuration;
using SiteKit.Areas.Email;
using SiteKit.Areas.Emails.Models;
using System.Net.Mail;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class EmailServiceCollectionExtensions
    {
        public static IServiceCollection AddSmtpClientMailer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDataProtection();
            services.Configure<EmailOptions>(configuration.GetSection("EmailOptions"));
            services.AddSingleton<IMailer, SmtpClientMailer>();

            return services;
        }
    }
}
