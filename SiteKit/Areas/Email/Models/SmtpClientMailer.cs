﻿using Microsoft.Extensions.Options;
using SiteKit.Areas.Email;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SiteKit.Areas.Emails.Models
{
    public class SmtpClientMailer : IMailer
    {
        private readonly EmailOptions emailOptions;

        public SmtpClientMailer(IOptions<EmailOptions> emailOptions)
        {
            this.emailOptions = emailOptions.Value;
        }

        public async Task SendAsync(MailMessage mailMessage)
        {
            using (var client = new SmtpClient
            {
                DeliveryMethod = emailOptions.DeliveryMethod,
            })
            {
                emailOptions.ConfigureSmtpClient(client);

                if (mailMessage.From == null)
                {
                    mailMessage.From = new MailAddress(emailOptions.FromAddress);
                }

                await client.SendMailAsync(mailMessage);
            }
        }
    }
}
