﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SiteKit.Areas.Emails.Models
{
    public interface IMailer
    {
        Task SendAsync(MailMessage mailMessage);
    }
}
