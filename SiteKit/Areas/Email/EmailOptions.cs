﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace SiteKit.Areas.Email
{
    public class EmailOptions : IValidatableObject
    {
        public SmtpDeliveryMethod DeliveryMethod => PickupDirectoryLocation != null ? SmtpDeliveryMethod.SpecifiedPickupDirectory : SmtpDeliveryMethod.Network;

        public string PickupDirectoryLocation { get; set; }

        /// <summary>
        /// Defaults to <c>true</c>.
        /// </summary>
        public bool EnableSsl { get; set; } = true;
        public string Host { get; set; }
        public int Port { get; set; }

        public bool UseDefaultCredentials { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        [Required]
        public string FromAddress { get; set; }

        public void ConfigureSmtpClient(SmtpClient client)
        {
            client.DeliveryMethod = DeliveryMethod;

            if (PickupDirectoryLocation != null)
                client.PickupDirectoryLocation = PickupDirectoryLocation;

            client.EnableSsl = EnableSsl;
            if (Host != null)
                client.Host = Host;
            if (Port > 0)
                client.Port = Port;

            if (UseDefaultCredentials)
            {
                client.UseDefaultCredentials = true;
            }
            else if (Username != null)
            {
                client.Credentials = new NetworkCredential(Username, Password);
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (PickupDirectoryLocation == null && Host == null)
            {
                yield return new ValidationResult("The pickup directory location or host must be specified.");
            }
        }
    }
}
