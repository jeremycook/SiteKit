﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class InstantAccessServiceCollectionExtensions
    {
        public static IServiceCollection AddInstantAccess(this IServiceCollection services, string authenticationScheme = CookieAuthenticationDefaults.AuthenticationScheme)
        {
            services
            .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(authenticationScheme, options =>
            {
                options.LoginPath = new PathString("/InstantAccess/Login");
                options.AccessDeniedPath = new PathString("/InstantAccess/AccessDenied");
                options.LogoutPath = new PathString("/InstantAccess/Logout");
                options.SlidingExpiration = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(14);
            });

            return services;
        }
    }
}
