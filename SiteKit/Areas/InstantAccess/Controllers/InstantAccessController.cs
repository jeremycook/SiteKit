﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using SiteKit.Areas.Crypto;
using SiteKit.Areas.Emails.Models;
using SiteKit.Areas.InstantAccess.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace SiteKit.Areas.InstantAccess
{
    [Area(nameof(InstantAccess))]
    [Route("[controller]/[action]")]
    [RequireHttps]
    [AllowAnonymous]
    public class InstantAccessController : Controller
    {
        private readonly IMailer mailer;
        private readonly IDataProtectionProvider protectionProvider;

        public InstantAccessController(IMailer mailer, IDataProtectionProvider protectionProvider)
        {
            this.mailer = mailer;
            this.protectionProvider = protectionProvider;
        }

        public IActionResult Login(string email = null)
        {
            var model = new Login
            {
                Email = email,
            };
            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(Login model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                using (var rng = RandomNumberGenerator.Create())
                {
                    var data = new byte[16];
                    rng.GetBytes(data);
                }
                var code = CodeGenerators.GenerateCrockfordBase32();

                // Send code
                await mailer.SendAsync(new System.Net.Mail.MailMessage
                {
                    To = { model.Email },
                    Subject = $"Login code for {Url.Link("home", null)}",
                    Body = code
                });

                // Encrypt code
                var protector = protectionProvider.CreateProtector(nameof(InstantAccessController), model.Email)
                    .ToTimeLimitedDataProtector();
                string hash = protector.Protect(code, TimeSpan.FromMinutes(5));

                return RedirectToAction(nameof(Confirm), new
                {
                    email = model.Email,
                    hash = hash,
                    returnUrl
                });
            }

            return View(model);
        }

        public IActionResult Confirm(string email = null, string hash = null)
        {
            var model = new Confirm
            {
                Email = email,
                Hash = hash,
            };
            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Confirm(Confirm model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var issuer = Url.Link("home", null);
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, model.Email, ClaimValueTypes.String, issuer),
                    new Claim(ClaimTypes.Name, model.Email, ClaimValueTypes.String, issuer),
                };
                var userIdentity = new ClaimsIdentity(claims, "InstantAccess");
                var userPrincipal = new ClaimsPrincipal(userIdentity);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    userPrincipal,
                    new AuthenticationProperties
                    {
                        IsPersistent = true,
                    });

                return GoToLocalUrl(returnUrl);
            }

            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }

        public IActionResult Denied()
        {
            return View();
        }

        private IActionResult GoToLocalUrl(string url)
        {
            if (Url.IsLocalUrl(url))
            {
                return Redirect(url);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
