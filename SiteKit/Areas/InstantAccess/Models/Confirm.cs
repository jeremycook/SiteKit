﻿using Microsoft.AspNetCore.DataProtection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SiteKit.Areas.InstantAccess.Models
{
    public class Confirm : IValidatableObject
    {
        [DataType("Hidden")]
        public string Email { get; set; }
        [DataType("Hidden")]
        public string Hash { get; set; }
        [Required]
        public string Code { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var protector = validationContext.GetDataProtector(nameof(InstantAccessController), Email)
                .ToTimeLimitedDataProtector();

            ValidationResult codeError = null;
            try
            {
                string expectedCode = protector.Unprotect(Hash);
                if (Code.Replace(" ", "") != expectedCode.Replace(" ", ""))
                {
                    codeError = new ValidationResult("Incorrect login code. Please verify that the code was entered correctly, or have a new code sent to you.");
                }
            }
            catch (CryptographicException)
            {
                codeError = new ValidationResult("The login code expired, or is invalid. Please verify that the code was entered correctly, or have a new code sent to you.", new[] { nameof(Code) });
            }
            if (codeError != null)
            {
                yield return codeError;
            }
        }
    }
}
