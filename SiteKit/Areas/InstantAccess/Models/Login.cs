﻿using System.ComponentModel.DataAnnotations;

namespace SiteKit.Areas.InstantAccess.Models
{
    public class Login
    {
        [Required]
        [EmailAddress]
        [StringLength(254)]
        public string Email { get; set; }
    }
}
