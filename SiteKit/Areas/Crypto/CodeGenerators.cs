﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SiteKit.Areas.Crypto
{
    public static class CodeGenerators
    {
        public static readonly char[] CrockfordBase32 = new[]
        {
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'J',
            'K',
            'M',
            'N',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'V',
            'W',
            'X',
            'Y',
            'Z',
        };

        public static string GenerateCrockfordBase32(int tokens = 16, int spacesEveryNTokens = 4)
        {
            using (var rng = RandomNumberGenerator.Create())
            {
                var data = new byte[tokens];
                rng.GetBytes(data);

                var code = string.Concat(data.Select(d => CrockfordBase32[(int)(32 * (d / 255f))]));
                code = Regex.Replace(code, ".{" + spacesEveryNTokens + "}", "$0 ");

                return code;
            }
        }
    }
}
