﻿using SiteKit.Areas.Messages;

namespace SiteKit.Areas.Storage
{
    public class Change : IMessage
    {
        public Change(string catalog, string id)
        {
            Catalog = catalog;
            Id = id;
        }

        public string Catalog { get; }
        public string Id { get; }
    }
}