﻿using System;

namespace SiteKit.Areas.Storage
{
    public class Put : Change
    {
        public Put(string catalog, string id, object model, Type typeHint)
            : base(catalog, id)
        {
            Model = model;
            TypeHint = typeHint;
        }

        public object Model { get; }
        public Type TypeHint { get; }
    }
}