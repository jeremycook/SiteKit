﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using SiteKit.Areas.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SiteKit.Areas.Storage
{
    public class Database
    {
        private readonly IMessenger messenger;

        public string DatabaseRootPath { get; }

        public Database(IHostingEnvironment environment, IMessenger messenger)
        {
            DatabaseRootPath = Path.Combine(Path.GetDirectoryName(environment.ContentRootPath), "Database");
            this.messenger = messenger;
        }

        public Queue<Change> Changes { get; } = new Queue<Change>();

        public async Task<object> GetAsync(string catalog, string id, Type type)
        {
            var path = Path.Combine(DatabaseRootPath, catalog, $"{id}.json");
            if (File.Exists(path))
            {
                var json = File.ReadAllText(path);
                return JsonConvert.DeserializeObject(json, type);
            }
            else
            {
                // Instead of file not found.
                return null;
            }
        }

        public async Task<IEnumerable> ListAsync(string catalog, Type typeHint)
        {
            var dir = Path.Combine(DatabaseRootPath, catalog);

            if (!Directory.Exists(dir))
            {
                return Enumerable.Empty<object>();
            }

            var list = new List<object>();
            foreach (var path in Directory.EnumerateFiles(dir))
            {
                var json = File.ReadAllText(path);
                list.Add(JsonConvert.DeserializeObject(json, typeHint));
            }
            return list;
        }

        public Catalog<T> OpenCatalog<T>(string name)
        {
            return new Catalog<T>(this, name);
        }

        public Catalog<T> OpenCatalog<T>()
        {
            return new Catalog<T>(this, typeof(T).Name);
        }

        public async Task SaveChangesAsync()
        {
            while (Changes.Count > 0)
            {
                var change = Changes.Dequeue();
                var path = Path.Combine(DatabaseRootPath, change.Catalog, $"{change.Id}.json");

                if (change is Put put)
                {
                    await messenger.SendMessageAsync($"Putting:{change.Catalog}", put);

                    string json = JsonConvert.SerializeObject(put.Model, put.TypeHint, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        Formatting = Formatting.Indented
                    });

                    // Ensure the catalog's directory exists.
                    Directory.CreateDirectory(Path.Combine(DatabaseRootPath, change.Catalog));
                    File.WriteAllText(path, json);

                    await messenger.SendMessageAsync($"Put:{change.Catalog}", put);
                }
                else if (change is Delete delete)
                {
                    await messenger.SendMessageAsync($"Deleting:{change.Catalog}", delete);
                    File.Delete(path);
                    await messenger.SendMessageAsync($"Deleted:{change.Catalog}", delete);
                }
                else
                {
                    throw new NotSupportedException(change.GetType()?.ToString());
                }
            }
        }
    }
}
