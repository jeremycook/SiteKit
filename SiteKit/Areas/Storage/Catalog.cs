﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteKit.Areas.Storage
{
    public class Catalog<T>
    {
        public Catalog(Database database, string name)
        {
            Database = database;
            Name = name;
            NormalizedName = name.ToLowerInvariant();

        }

        public Database Database { get; }
        public string Name { get; }
        public string NormalizedName { get; private set; }

        public void Put(string id, T model)
        {
            Database.Changes.Enqueue(new Put(NormalizedName, id, model, typeof(T)));
        }

        public void Delete(string id)
        {
            Database.Changes.Enqueue(new Delete(NormalizedName, id));
        }

        public async Task<T> GetAsync(string id)
        {
            var model = await Database.GetAsync(NormalizedName, id, typeof(T));
            if (model is T t)
            {
                return t;
            }
            else
            {
                // BUG?
                return default(T);
            }
        }

        public async Task<IEnumerable<T>> ListAsync()
        {
            var list = await Database.ListAsync(NormalizedName, typeof(T));
            return list.Cast<T>();
        }
    }
}