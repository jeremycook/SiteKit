﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

namespace ExampleSite
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
            .AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddSeq();
            });

            services
            .AddSmtpClientMailer(configuration)
            .AddInstantAccess();

            services
            .AddMvc(o =>
            {
            })
            .AddApplicationPart(typeof(SiteKit.ApplicationPart).Assembly)
            .AddRazorOptions(options =>
            {
                options.FileProviders.Add(new EmbeddedFileProvider(typeof(SiteKit.ApplicationPart).Assembly));
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes
                .MapRoute(
                    name: "home",
                    template: "",
                    defaults: new { controller = "Home", action = "Index" })
                .MapRoute(
                    name: "controller/action",
                    template: "{controller}/{action=Index}");
            });
        }
    }
}
